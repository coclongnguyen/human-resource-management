package com.example.demoMySql.data.service;

import com.example.demoMySql.data.model.Employee;
import com.example.demoMySql.data.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeServiceImp {

    @Autowired
    private EmployeeRepository employeeRepository;

    public void addEmployee(Employee employee){
        employeeRepository.save(employee);
    }

//    public Employee findOne(int id){
//         return employeeRepository.findOne(id);
//    }

    public boolean updateEmployee(Employee employee){
        try{
            employeeRepository.save(employee);
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public List<Employee> getListAllEmployee(){
        try{
            return employeeRepository.findAll();
        }catch (Exception e){
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public boolean deleteEmployee(int id){
        try{
            employeeRepository.deleteById(id);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
