package com.example.demoMySql.controller;

import com.example.demoMySql.data.model.Employee;
import com.example.demoMySql.data.service.EmployeeServiceImp;
import com.example.demoMySql.view.EmployeeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
public class EmployeeController {

    @Autowired
    private EmployeeServiceImp employeeServiceImp;

    @GetMapping("/employee")
    public List<Employee> getAllEmployee(){
        List<Employee> employeeList = (List<Employee>) employeeServiceImp.getListAllEmployee();
        return employeeList;
    }

    @PostMapping("/create")
    public ResponseEntity addEmployee(@RequestBody EmployeeDTO dto){
        Employee employee = new Employee();

        employee.setId(dto.getId());
        employee.setName(dto.getName());
        employee.setGender(dto.getGender());
        employee.setAvatar(dto.getAvatar());
        employee.setEmail(dto.getEmail());
        employee.setPhoneNumber(dto.getPhoneNumber());
        employee.setAddress(dto.getAddress());

        employeeServiceImp.addEmployee(employee);
        return ResponseEntity.ok().body(employee);
    }

//    @PutMapping("/employee/{id}")
//    public Employee updateEmployee(@PathVariable int id, EmployeeDTO dto){
//        Employee employee = employeeService.findOne(id);
//
//        employee.setId(dto.getId());
//        employee.setName(dto.getName());
//        employee.setGender(dto.getGender());
//        employee.setAvatar(dto.getAvatar());
//        employee.setEmail(dto.getEmail());
//        employee.setPhoneNumber(dto.getPhoneNumber());
//        employee.setAddress(dto.getAddress());
//
//        return employee;
//    }


}
